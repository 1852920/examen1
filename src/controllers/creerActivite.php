<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

class creerActivite
{
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation

    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    private static function estValide(string $chaine): bool
    {
        $re = '/\D/';
        $str = $chaine;

        return(preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0));
    }


    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide

        // Je ne sais pas comment faire, désolé.
        if (self::estValide($_POST['lieu']) === 1 && self::estValide($_POST['partenaire']) === 1
            && self::estValide($_POST['activite']) === 1 && self::estValide($_POST['determinants']) === 1
            && self::estValide($_POST['duree']) === 1 && self::estValide($_POST['motivation']) === 1
            && self::estValide($_POST['plaisir']) === 1)
        {
            //Si valide, créer l'activité
            $activite = new Activite($details);
            //@TODO Si valide, l'ajouter au tableau des activités
            $creation = self::getEquipe()->addActivite($activite);
            $_SESSION["activites"]["$activite"];
        }
        else{
            $creation = false;
        }
        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}