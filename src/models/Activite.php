<?php


class Activite
{
    private string $lieu;
    private string $date;
    private string $partenaire;
    private string $activite;
    private string $determinant;
    private string $intensite;
    private string $duree;
    private string $effet;
    private string $motivation;
    private string $plaisir;

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getPartenaire(): string
    {
        return $this->partenaire;
    }

    /**
     * @param string $partenaire
     */
    public function setPartenaire(string $partenaire): void
    {
        $this->partenaire = $partenaire;
    }

    /**
     * @return string
     */
    public function getActivite(): string
    {
        return $this->activite;
    }

    /**
     * @param string $activite
     */
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }

    /**
     * @return string
     */
    public function getDeterminant(): string
    {
        return $this->determinant;
    }

    /**
     * @param string $determinant
     */
    public function setDeterminant(string $determinant): void
    {
        $this->determinant = $determinant;
    }

    /**
     * @return string
     */
    public function getIntensite(): string
    {
        return $this->intensite;
    }

    /**
     * @param string $intensite
     */
    public function setIntensite(string $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return string
     */
    public function getDuree(): string
    {
        return $this->duree;
    }

    /**
     * @param string $duree
     */
    public function setDuree(string $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return string
     */
    public function getEffet(): string
    {
        return $this->effet;
    }

    /**
     * @param string $effet
     */
    public function setEffet(string $effet): void
    {
        $this->effet = $effet;
    }

    /**
     * @return string
     */
    public function getMotivation(): string
    {
        return $this->motivation;
    }

    /**
     * @param string $motivation
     */
    public function setMotivation(string $motivation): void
    {
        $this->motivation = $motivation;
    }

    /**
     * @return string
     */
    public function getPlaisir(): string
    {
        return $this->plaisir;
    }

    /**
     * @param string $plaisir
     */
    public function setPlaisir(string $plaisir): void
    {
        $this->plaisir = $plaisir;
    }
}